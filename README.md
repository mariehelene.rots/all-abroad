# All Abroad

All Abroad is a school project for my second year of Mastère at ECV Digital Nantes. <br/>
The goal is to create a javascript application which gamify a service of our choice.

## Problematic

### Observation

Let's talk about two possible scenarios:

> I have a visio-conference with a Bresilian and he ask me if I hear him well. I'm answering with “👌”... He hang up? 🧐

-> “👌”, for Bresilians, has a stronger meaning than a middle finger.

> I'm moving to South Korea and my new appartment is at the fourth floor... There is no fourth floor in the elevator? 🧐

-> The 4th floor is always replaced by "F" in korean elevators because of tetraphobia: ‘사 (the number 4 in Sino-Korean)’ and the Sino-Korean word for ‘사 (death)’ are homophones.

In this two cases, learning the language of the person/country was maybe a good idea but learning the **culture** too could have been really helpful!

so... **What is the place of culture in learning a language?**

## Learn more about it...
- [statement of intent (🚨 in French)](https://www.figma.com/proto/M1MMXfjeDrV7Kj7J5yfZVW/All-Abroad?page-id=466%3A3207&node-id=473%3A3371&viewport=200%2C77%2C0.3435804545879364&scaling=min-zoom)
- [Figma basic prototype](https://www.figma.com/proto/M1MMXfjeDrV7Kj7J5yfZVW/All-Abroad?page-id=838%3A92&node-id=838%3A1051&viewport=288%2C-1316%2C0.12291432172060013&scaling=min-zoom)
