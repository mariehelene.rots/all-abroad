import React, { useState } from 'react';
import firebase from "firebase/app";
import {
    IonContent,
    IonPage,
    IonText,
    IonButton,
    IonToggle,
    IonItem,
    IonLabel,
    IonInput,
    IonIcon
} from '@ionic/react';
import { logoGoogle } from 'ionicons/icons'
import { ChevronRight, Mail, Unlock } from 'react-feather';
import { BrandPresentation } from "../components/brand/presentation"
import { auth } from "../firebase";
import styled from 'styled-components';


const InputContainer = styled(IonItem)({
    backgroundColor: "white",
    border: "1px solid #EFEFEF",
    boxSizing: "border-box",
    borderRadius: "15px",
    marginTop: "25px",
    marginBottom: "15px"
});

const IconContainer = styled("div")({
    margin: "auto 1em auto auto"
});

const Wrapper = styled("div")({
    maxWidth: "25em", margin: "auto"
});

export const Login = (): React.ReactElement => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(null);

    const signInWithEmailAndPasswordHandler =
        (event: any, email: string, password: string) => {
            event.preventDefault();
        };

    const onChangeHandler = (event: any) => {
        const { name, value } = event.currentTarget;

        if (name === 'userEmail') {
            setEmail(value);
        }
        else if (name === 'userPassword') {
            setPassword(value);
        }
    };

    const SignInWithGoogle = () => {
        const provider = new firebase.auth.GoogleAuthProvider();
        auth.signInWithPopup(provider)
    }

    return (
        <IonPage className="secondary-background">
            <IonContent className="ion-padding">
                <Wrapper>
                    <BrandPresentation />
                    <IonText>
                        <h1> Log In </h1>
                        <p>Enter your email and password</p>
                    </IonText>
                    <InputContainer lines="none" color="none" >
                        <IconContainer slot="start">
                            <Mail />
                        </IconContainer>
                        <IonLabel position="stacked">Email</IonLabel>
                        <IonInput
                            type="email"
                            id="userEmail"
                            name="userEmail"
                            value={email}
                            placeholder="anna@email.com"
                            onChange={(event) => onChangeHandler(event)}
                        />
                    </InputContainer>
                    <InputContainer lines="none" color="none" >
                        <IconContainer slot="start" >
                            <Unlock />
                        </IconContainer>
                        <div>
                            <IonLabel position="stacked">Password</IonLabel>
                            <IonInput
                                type="password"
                                name="userPassword"
                                id="userPassword"
                                value={password}
                                placeholder="•••••••••••••••••••••••"
                                onChange={(event) => onChangeHandler(event)}
                            />
                        </div>
                    </InputContainer>
                    <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
                        <div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                            <IonToggle value="Remember" name="Remember" color="secondary" />
                            <IonLabel> Remember </IonLabel>
                        </div>
                        <IonButton fill="clear" color="danger" size="small" strong={true}> Forgot password! </IonButton>
                    </div>
                    <IonButton
                        className="button"
                        type="submit"
                        color="primary"
                        expand="block"
                        style={{ marginBottom: "15px" }}
                        onClick={(event) => { signInWithEmailAndPasswordHandler(event, email, password) }}

                    >
                        Log In
                    </IonButton>
                    <IonButton className="button" color="secondary" expand="block" onClick={SignInWithGoogle} >
                        <IonIcon slot="start" icon={logoGoogle} />
                    Log In with Google
                    </IonButton>
                    <div style={{ display: "flex", flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                        <IonText> Don't have an account? </IonText>
                        <IonButton fill="clear" href="/sign_up"> Sign Up <ChevronRight /> </IonButton>
                    </div>
                </Wrapper>
            </IonContent>
        </IonPage >
    );
}