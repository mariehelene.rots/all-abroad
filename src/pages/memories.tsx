import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import styled from 'styled-components';

const StyledHeader = styled(IonHeader)`
   ion-toolbar {
        border-radius: 0px 0px 16px 16px;
    }
`;
export const MemoriesPage: React.FC = () => {
    return (
        <IonPage className="secondary-background">
            <StyledHeader className="ion-no-border">
                <IonToolbar>
                    <IonTitle>Your memories</IonTitle>
                </IonToolbar>
            </StyledHeader>
            <IonContent className="ion-padding">
                memories
            </IonContent>
        </IonPage>
    );
};