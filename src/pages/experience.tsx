import * as THREE from 'three';
import { IonPage } from '@ionic/react';
import React, { Suspense, useRef } from 'react';
import { Canvas, extend, useFrame, useThree, useLoader, ReactThreeFiber } from 'react-three-fiber';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { hideTab } from "../helpers/hide-tab";

// Set right types for OrbitControls
declare global {
    namespace JSX {
        interface IntrinsicElements {
            orbitControls: ReactThreeFiber.Object3DNode<
                OrbitControls,
                typeof OrbitControls
            >;
        }
    }
}

interface OrbitRef {
    obj: {
        update: Function;
    };
}

extend({ OrbitControls });

const Controls: React.FC<any> = (props) => {
    const ref = useRef<OrbitRef>(null);
    const { camera, gl } = useThree();
    useFrame(() => {
        ref.current?.obj?.update();
    });
    return <orbitControls ref={ref} args={[camera, gl.domElement]} {...props} />;
};

const Dome: React.FC<any> = (props) => {
    const texture = useLoader(THREE.TextureLoader, 'https://lh3.googleusercontent.com/p/AF1QipOE9smjtA2rueOtUuIY9vCvCiA7fLYVexNyi19Z=w6000')
    return (
        <mesh>
            <sphereBufferGeometry attach="geometry" args={[500, 60, 40]} />
            <meshBasicMaterial attach="material" map={texture} side={THREE.BackSide} {...props} />
        </mesh>
    )
}

/**
 * Canvas Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}
export const ExperiencePage: React.FC = () => {
    //Hide TabBar on this specific page
    hideTab();
    return (
        <IonPage>
            <Canvas camera={{ position: [0, 0, 0.1] }} style={{ transform: "scaleX(-1)", width: sizes.width, height: sizes.height }}>
                <ambientLight />
                <pointLight position={[10, 10, 10]} />
                <Controls enableZoom={true} />
                <Suspense fallback={null}>
                    <Dome />
                </Suspense>
            </Canvas>
        </IonPage>
    );
};