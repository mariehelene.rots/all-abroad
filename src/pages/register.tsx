import React from 'react';
import firebase from "firebase/app";
import {
    IonContent,
    IonPage,
    IonText,
    IonButton,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonIcon
} from '@ionic/react';
import { BrandPresentation } from "../components/brand/presentation"
import { ChevronRight } from 'react-feather';
import { logoGoogle } from 'ionicons/icons'
import { auth } from "../firebase";

const SignUpWithGoogle = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    auth.signInWithPopup(provider)
}
export const Register = (): React.ReactElement => {
    return (
        <IonPage className="secondary-background">
            <IonContent className="ion-padding">
                <BrandPresentation />
                <IonText>
                    <h1> Sign Up </h1>
                    <p>Enter your credentials to continue</p>
                </IonText>
                <IonList>
                    <IonItem lines="none" color="none">
                        <IonLabel position="stacked">Name</IonLabel>
                        <IonInput value={null} placeholder="Anna" />
                    </IonItem>
                    <IonItem lines="none" color="none">
                        <IonLabel position="stacked">Email</IonLabel>
                        <IonInput value={null} placeholder="anna@email.com" type="email" />
                    </IonItem>
                    <IonItem lines="none" color="none">
                        <IonLabel position="stacked">Password</IonLabel>
                        <IonInput value={null} placeholder="•••••••••••••••••••••••" type="password" />
                    </IonItem>
                    <IonItem lines="none" color="none">
                        <IonLabel position="stacked">Confirm Password</IonLabel>
                        <IonInput value={null} placeholder="•••••••••••••••••••••••" type="password" />
                    </IonItem>
                </IonList>
                <IonButton color="primary" expand="block" type="submit"> Sign Up </IonButton>
                <IonButton color="secondary" expand="block" onClick={SignUpWithGoogle} > <IonIcon slot="start" icon={logoGoogle} /> Sign Up with Google</IonButton>
                <IonItem lines="none" color="none">
                    <IonText> Already have an account? </IonText>
                    <IonButton fill="clear" href="/login"> Login <ChevronRight /> </IonButton>
                </IonItem>
            </IonContent>
        </IonPage>
    );
};