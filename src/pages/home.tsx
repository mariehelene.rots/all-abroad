import React from 'react';
import { IonContent, IonPage, IonText } from '@ionic/react';
import { Settings, Save, Hexagon, MessageCircle } from 'react-feather';
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from '../firebase';
import { Card } from '../components/card';
import { showTab } from "../helpers/hide-tab";

export const HomePage: React.FC = () => {
    const [user] = useAuthState(auth);
    showTab();
    return (
        <IonPage className="secondary-background">
            <IonContent className="ion-padding">
                <div style={{
                    display: "flex",
                    justifyContent: "flex-end"
                }}>
                    <Settings />
                </div>
                <div style={{
                    display: "flex",
                    flexDirection: "column",
                    marginTop: "4vh"
                }}>
                    <IonText> {user?.displayName} 환영합니다 ! </IonText>
                    <IonText style={{
                        fontWeight: 400,
                        fontStyle: "normal",
                        fontSize: "12px",
                        lineHeight: "16px",
                        color: "#8F92A1",
                        marginBottom: "8vh"
                    }}>
                        Welcome back {user?.displayName}!
                        </IonText>
                    <Card
                        icon={<Save size="15" />}
                        title="You are almost there!"
                        subtitle="Chapter 1 - Lost in the airport"
                        content="Just after landing in Seoul you discovered you have lost your wallet. Let’s find some clues to found it back before starting you travel through South Korea!"
                        buttonText="Start"
                        buttonLink="/experience"
                    />
                    <div style={{ display: "flex", flexDirection: "row" }}>

                        <Card
                            icon={<Hexagon size="15" />}
                            title="Language"
                            subtitle="Let’s revise some vocabulary, grammar, ..."
                            buttonText="Coming Soon"
                        />
                        <Card
                            icon={<MessageCircle size="15" />}
                            title="Small Talk"
                            subtitle="Let’s talk about anything and everything"
                            buttonText="Coming Soon"
                        />
                    </div>
                </div>
            </IonContent>
        </IonPage>
    );
};