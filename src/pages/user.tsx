import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButton } from '@ionic/react';
import styled from 'styled-components';
import { auth } from '../firebase';

const StyledHeader = styled(IonHeader)`
   ion-toolbar {
        border-radius: 0px 0px 16px 16px;
    }
`;
export const UserPage: React.FC = () => {
    return (
        <IonPage className="secondary-background">
            <StyledHeader className="ion-no-border">
                <IonToolbar>
                    <IonTitle>You</IonTitle>
                </IonToolbar>
            </StyledHeader>
            <IonContent className="ion-padding">
                USER PAGE
                 <IonButton className="button" onClick={() => { auth.signOut() }}>
                    Log Out
                </IonButton>
            </IonContent>
        </IonPage>
    );
};