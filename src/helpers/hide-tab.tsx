export const hideTab = () => {
  const tabBar = document.querySelectorAll('.appTabBar')[0] as HTMLElement;
  if (tabBar !== null) {
    tabBar.style.display = 'none';
  }
}

export const showTab = () => {
  const tabBar = document.querySelectorAll('.appTabBar')[0] as HTMLElement;
  if (tabBar !== null) {
    tabBar.style.display = 'flex';
  }
}
