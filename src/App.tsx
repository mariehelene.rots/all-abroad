import { IonApp, IonTabBar, IonTabs, IonTabButton, IonLabel } from '@ionic/react';
import { Home, BookOpen, Play, Film, User } from 'react-feather';
import { IonReactRouter } from '@ionic/react-router';
import { Redirect, Route } from 'react-router-dom';
import { IonRouterOutlet } from '@ionic/react';
import styled from 'styled-components';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

/* Firebase imports */
import "firebase/firestore";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from './firebase';
import { HomePage } from './pages/home';
import { Login } from './pages/login';
import { UserPage } from './pages/user';
import { MemoriesPage } from './pages/memories';
import { ChaptersPage } from './pages/chapters';
import { ExperiencePage } from './pages/experience';
import { Register } from './pages/register';

const StyledTabBar = styled(IonTabs)`
ion-tab-bar {
    border-radius: 20px 20px 0px 0px;
    border: none;
    height: 73px;
}
`;

const LoggedInApp = () => (
  <StyledTabBar>
    <IonRouterOutlet>
      <Route exact path="/">
        <HomePage />
      </Route>
      <Route exact path="/chapters">
        <ChaptersPage />
      </Route>
      <Route path="/experience">
        <ExperiencePage />
      </Route>
      <Route exact path="/memories">
        <MemoriesPage />
      </Route>
      <Route exact path="/user">
        <UserPage />
      </Route>
      <Route exact path="/login">
        <Redirect to="/" />
      </Route>
      <Route exact path="/sign_up">
        <Redirect to="/" />
      </Route>
    </IonRouterOutlet>
    <IonTabBar slot="bottom" className="appTabBar">
      <IonTabButton tab="home" href="/">
        <Home />
        <IonLabel>Home</IonLabel>
      </IonTabButton>
      <IonTabButton tab="chapters" href="/chapters">
        <BookOpen />
        <IonLabel>Chapters</IonLabel>
      </IonTabButton>
      <IonTabButton tab="experience" href="/experience">
        <Play />
        <IonLabel>Play</IonLabel>
      </IonTabButton>
      <IonTabButton tab="memories" href="/memories">
        <Film />
        <IonLabel>Memories</IonLabel>
      </IonTabButton>
      <IonTabButton tab="user" href="/user">
        <User />
        <IonLabel>User</IonLabel>
      </IonTabButton>
    </IonTabBar>
  </StyledTabBar>
);

const NotLoggedInApp = () => (
  <IonRouterOutlet>
    <Route exact path="/login">
      <Login />
    </Route>
    <Route exact path="/sign_up">
      <Register />
    </Route>
    <Route exact path="/">
      <Redirect to="/login" />
    </Route>
    <Route exact path="/memories">
      <Redirect to="/login" />
    </Route>
    <Route exact path="/chapters">
      <Redirect to="/login" />
    </Route>
    <Route exact path="/experience">
      <Redirect to="/login" />
    </Route>
    <Route exact path="/user">
      <Redirect to="/login" />
    </Route>
  </IonRouterOutlet>
);

const App: React.FC = () => {
  const [user] = useAuthState(auth);
  return (
    <IonApp>
      <IonReactRouter >
        {user ?
          <LoggedInApp /> :
          <NotLoggedInApp />}
      </IonReactRouter>
    </IonApp>
  )
};

export default App;