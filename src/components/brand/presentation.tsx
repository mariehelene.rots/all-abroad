import React from 'react';
import {
    IonText,
    IonImg,
} from '@ionic/react';
import styled from 'styled-components';

const Logo = styled(IonImg)`
width:50px;
height:50px;
margin-left:auto;
margin-right:auto;
`;

const LogoWrapper = styled("div")`
display:flex;
flex-direction:column;
text-align:center;
align-items:center;
margin-left:auto;
margin-right:auto;
`;


export const BrandPresentation = (): React.ReactElement => {
    return (
        <LogoWrapper>
            <Logo src={"assets/icon/icon.png"} alt="All Abroad Logo which is a blue air balloon" />
            <IonText>
                <h1> All Abroad </h1>
            </IonText>
            <IonText>
                <p>
                    Learn to interact through <br /> the prism of culture
                </p>
            </IonText>
        </LogoWrapper>
    );
};