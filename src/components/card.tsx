import React from 'react';
import { IonButton, IonText } from '@ionic/react';
import styled from 'styled-components';

const Wrapper = styled("div")`
  background-color: white;
  border-radius: 14px;
  padding: 1em;
  margin: 0.5em;
  box-shadow: 0px 8px 24px rgba(149, 157, 165, 0.2);
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Header = styled("div")`
  display: flex;
  flex-direction: row;
  align-items: center;
  text-transform: uppercase;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 24px;
`;

const Subtitle = styled(IonText)`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
  color: #8F92A1;
`;

const Content = styled(IonText)`
  font-size: 14px;
  line-height: 18px;
  color: #4F4F4F;
  margin-top: 0.5em;
`;

const Button = styled(IonButton)`
  width: fit-content;
  margin-left: auto;
  margin-right: 0px;
  margin-top: 1em;
`;
type Props = {
  icon: React.ReactElement;
  title: string;
  subtitle: string;
  content?: string | undefined;
  buttonText: string;
  buttonLink?: string | undefined;
}
export const Card: React.FC<Props> = (
  {
    icon,
    title,
    subtitle,
    content,
    buttonText,
    buttonLink }: Props) => {

  return (
    <Wrapper>
      <Header>
        {icon}
        <IonText style={{ marginLeft: "0.5em" }}> {title} </IonText>
      </Header>
      <Subtitle> {subtitle} </Subtitle>
      { content ? <Content> {content} </Content> : null}
      <Button
        className="button"
        href={buttonLink}
        disabled={buttonLink !== undefined ? false : true}

      >
        {buttonText}
      </Button>
    </Wrapper >
  );
};